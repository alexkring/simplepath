﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RVO;

public class RVO2Simulation : MonoBehaviour 
{
	private Dictionary<int,SteeringAgentComponent> _steeringAgentDict;
	private Dictionary<int,FootprintComponent> _obstacleDict;
	
	private void Awake()
	{
		_steeringAgentDict = new Dictionary<int,SteeringAgentComponent>();
		_obstacleDict = new Dictionary<int,FootprintComponent>();
	}
	
	private void Start()
	{	
		Simulator.Instance.setAgentDefaults(
			10f,
			10,
			2f,
			2f,
			0.5f,
			4f,
			new RVO.Vector2(0f,0f) );
			
		SteeringAgentComponent[] steeringAgentArray = GameObject.FindObjectsOfType<SteeringAgentComponent>();
		for ( int i = 0; i < steeringAgentArray.Length; i++ )
		{	
			SteeringAgentComponent agent = steeringAgentArray[i];
			int rvoId = Simulator.Instance.addAgent( new RVO.Vector2( agent.transform.position.x, agent.transform.position.z ) );
			_steeringAgentDict.Add( rvoId, agent );
			
			Agent rvoAgent = Simulator.Instance.agents_[rvoId];
			rvoAgent.maxSpeed_ = agent.m_maxSpeed;
			rvoAgent.radius_ = agent.m_radius;
		}

		FootprintComponent[] footprintArray = GameObject.FindObjectsOfType<FootprintComponent>();
		for ( int i = 0; i < footprintArray.Length; i++ )
		{	
			FootprintComponent footprint = footprintArray[i];
			Bounds fpBounds = footprint.GetBounds();
			
			List<RVO.Vector2> rvoObstacleVertices = new List<RVO.Vector2>( 4 );
			rvoObstacleVertices.Add( new RVO.Vector2( fpBounds.min.x, fpBounds.min.z ) );
			rvoObstacleVertices.Add( new RVO.Vector2( fpBounds.max.x, fpBounds.min.z ) );
			rvoObstacleVertices.Add( new RVO.Vector2( fpBounds.max.x, fpBounds.max.z ) );
			rvoObstacleVertices.Add( new RVO.Vector2( fpBounds.min.x, fpBounds.max.z ) );
			//rvoObstacleVertices.Add( new RVO.Vector2( fpBounds.min.x, fpBounds.min.z ) );
			
			int rvoObstacleId = Simulator.Instance.addObstacle( rvoObstacleVertices );
			_obstacleDict.Add( rvoObstacleId, footprint );
		}
		
		Simulator.Instance.processObstacles();
	}
	
	private void Update()
	{
		// Update preferred velocity
		foreach ( KeyValuePair<int,SteeringAgentComponent> kvp in _steeringAgentDict )
		{
			SteeringAgentComponent agent = kvp.Value;
			
			agent.DoUpdate( Time.deltaTime );
			
			//Vector3 desiredDir = agent.DesiredVelocity.normalized;
			//RVO.Vector2 rvoPreferredVelocity = new RVO.Vector2( desiredDir.x, desiredDir.z );
			//RVO.Vector2 rvoPosition = new RVO.Vector2( agent.transform.position.x, agent.transform.position.z );
			RVO.Vector2 rvoPreferredVelocity = new RVO.Vector2( agent.DesiredVelocity.x, agent.DesiredVelocity.z );
			
			Agent rvoAgent = Simulator.Instance.agents_[kvp.Key];
			rvoAgent.prefVelocity_ = rvoPreferredVelocity;
			//rvoAgent.position_ = rvoPosition;
		}
		
		// Update RVO simulation
		Simulator.Instance.setTimeStep( Time.deltaTime );
		Simulator.Instance.doStep();
		
		// Translate
		foreach ( KeyValuePair<int,SteeringAgentComponent> kvp in _steeringAgentDict )
		{
			SteeringAgentComponent agent = kvp.Value;
			//RVO.Vector2 rvoVel = Simulator.Instance.getAgentVelocity( kvp.Key );
			//Vector3 velocity = new Vector3( rvoVel.x(), 0f, rvoVel.y() );
			RVO.Vector2 rvoAgentPos = Simulator.Instance.getAgentPosition( kvp.Key );
			Vector3 agentPos = new Vector3( rvoAgentPos.x(), agent.transform.position.y, rvoAgentPos.y() );
			agent.transform.position = agentPos;
		}
	}
}
