﻿#region Copyright
// ******************************************************************************************
//
// 							SimplePath, Copyright © 2011, Alex Kring
//
// ******************************************************************************************
#endregion

using UnityEngine;
using System.Collections;
using System;

namespace SimpleAI
{

	/// <summary>
	/// Represents stateful information about each cell of a grid.
	/// </summary>
	public class GridOccupancyMap 
	{
		public enum eOccupancyType
		{
			kObstacle = 0,
			kPathWeight,
			kCustom
		}
	
		private string _name; 					// For debugging purposes.
		private eOccupancyType _mapType;		// Type of map.
		private int[,] _occupancyArray;  		// Counts the number of things that are occupying each cell.
		private float _weight;					// Only set for weighted map types.
			
		public string Name
		{
			get { return _name; }
		}

		public int[,] OccupancyArray
		{
			get { return _occupancyArray; }
		}
		
		public float Weight
		{
			get { return _weight; }
			set { _weight = value; }
		}
		
		public eOccupancyType OccupancyType
		{
			get { return _mapType; }
		}
		
		public GridOccupancyMap( eOccupancyType mapType, string name, int numRows, int numCols, float weight = 0.0f )
		{
			_mapType = mapType;
			_name = name;
			_weight = weight;
			_occupancyArray = new int[numCols, numRows];
			Clear();
		}
		
		public void Clear()
		{
			Array.Clear( _occupancyArray, 0, _occupancyArray.Length );
		}
	}
	
} // namespace SimpleAI