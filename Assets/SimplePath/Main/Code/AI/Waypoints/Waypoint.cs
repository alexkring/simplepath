﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Waypoint : MonoBehaviour
{	
	#region Constants
	private const float kEmptyCellWeight = 1f;
	#endregion
	
	#region Fields
	private int _index;
	private float _weight;
	private bool _isBlocked;
	private List<int> _neighborList;
	private int[] _cachedNeigborArray;
	#endregion
	
	#region Properties
	public int Index
	{
		get { return _index; }
	}
		
	public bool IsBlocked
	{
		get { return _isBlocked; }
	}
	
	public float Weight
	{
		get { return _weight; }
		set { _weight = value; }
	}
	
	public int[] Neighbors
	{
		get { return _cachedNeigborArray; }
	}
	#endregion
	
	public void Initialize(int index)
	{
		_index = index;
		_weight = kEmptyCellWeight;
		_neighborList = new List<int>();
	}
	
	public void AddNeighbor( int neighbor )
	{
		_neighborList.Add( neighbor );
		_cachedNeigborArray = _neighborList.ToArray();
	}
	
	public void RemoveNeighbor( int neighbor )
	{
		_neighborList.Remove( neighbor );
		_cachedNeigborArray = _neighborList.ToArray();
	}
}
