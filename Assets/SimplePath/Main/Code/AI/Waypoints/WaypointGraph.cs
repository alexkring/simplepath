﻿using UnityEngine;
using System.Collections;
using UnityEngine;
using System;
using System.Collections.Generic;
using SimpleAI.Navigation;
using KDTree;

public class WaypointGraph : IPathTerrain
{
	#region Constants
	private const int kNumKdTreeSortingDimensions = 3;
	private const int kDefaultKdTreeBucketCapacity = 10;
	#endregion
	
	#region Fields
	private Dictionary<int,Waypoint> _waypointDict;
	private KDTree<Waypoint> _waypointTree;
	#endregion
	
	public WaypointGraph()
	{
		
	}
	
	public void AddWaypoint(Waypoint waypoint)
	{
		double[] kdPoint = ConvertToKdPoint( waypoint.transform.position );
		_waypointTree.AddPoint( kdPoint, waypoint );
		_waypointDict.Add(waypoint.Index, waypoint);
	}
	
	public void Build( Waypoint[] waypointArray, int maxNumNeighbors, float maxDistBetweenNeighbors )
	{
		_waypointDict = new Dictionary<int, Waypoint>();
		_waypointTree = new KDTree<Waypoint>( kNumKdTreeSortingDimensions, kDefaultKdTreeBucketCapacity );
		
		for ( int i = 0; i < waypointArray.Length; i++ )
		{
			waypointArray[i].Initialize( i );
			AddWaypoint( waypointArray[i] );
		}
		
		for ( int i = 0; i < waypointArray.Length; i++ )
		{
			Waypoint waypoint = waypointArray[i];
			double[] searchPoint = ConvertToKdPoint( waypoint.transform.position );
			NearestNeighbour<Waypoint> nearestNeighborWaypoint = _waypointTree.NearestNeighbors(
				searchPoint,
				maxNumNeighbors, 
				maxDistBetweenNeighbors );
			while ( nearestNeighborWaypoint.MoveNext() )
			{
				Waypoint neighbor = nearestNeighborWaypoint.Current;
				if ( neighbor == waypoint )
				{
					continue;
				}
				
				waypoint.AddNeighbor( neighbor.Index );
			}
		}
	}
	
	public void OnDrawGizmos(Color nodeColor, Color edgeColor, float nodeRadius)
	{
		foreach ( Waypoint waypoint in _waypointDict.Values )
		{
			Gizmos.color = nodeColor;
			Gizmos.DrawWireSphere( waypoint.transform.position, nodeRadius ); 
			
			Gizmos.color = edgeColor;
			for ( int i = 0; i < waypoint.Neighbors.Length; i++ )
			{
				Waypoint neighbor = GetWaypoint( waypoint.Neighbors[i] );
				Gizmos.DrawLine( waypoint.transform.position, neighbor.transform.position );
			}
		}
	}

	#region IPathTerrain Members
	public int GetNeighbors(int index, ref int[] neighbors)
	{
		neighbors = GetWaypoint(index).Neighbors;
		return neighbors.Length;
	}
	
	public int GetNumNodes()
	{
		return _waypointDict.Count;
	}
	
	private float GetDistanceCost(int startIndex, int goalIndex)
	{
		Waypoint startWaypoint = GetWaypoint(startIndex);
		Waypoint goalWaypoint = GetWaypoint(goalIndex);
		float distance = ( startWaypoint.transform.position - goalWaypoint.transform.position ).magnitude;
		float cost = distance;
		return cost;
	}
	
	public float GetGCost(int currentNodeIndex, int neighborNodeIndex)
	{
		float weight = GetPathWeight( neighborNodeIndex );
		float distanceCost = GetDistanceCost( currentNodeIndex, neighborNodeIndex );
		float cost = distanceCost * weight;
		return cost;
	}
	
	public float GetHCost(int startIndex, int goalIndex)
	{
		float cost = GetDistanceCost(startIndex, goalIndex);
		float heuristicWeight = 1.0f;
		cost *= heuristicWeight;
		return cost;
	}
	
	public bool IsNodeBlocked(int index)
	{
		return GetWaypoint(index).IsBlocked;
	}
	
	public Vector3 GetPathNodePos(int index)
	{
		return GetWaypoint(index).transform.position;
	}
	
	public int GetNearestPathNodeIndex(Vector3 pos)
	{
		if ( _waypointDict.Count == 0 )
		{
			return SimpleAI.Planning.Node.kInvalidIndex;
		}
			
		double[] searchPoint = ConvertToKdPoint( pos );
		
		const int kMaxNumNeighborsReturned = 1;
		NearestNeighbour<Waypoint> nearestNeighborWaypoint = _waypointTree.NearestNeighbors( searchPoint, kMaxNumNeighborsReturned );
		
		nearestNeighborWaypoint.MoveNext();
		return nearestNeighborWaypoint.Current.Index;
	}
	
	public void ComputePortalsForPathSmoothing(Vector3[] roughPath, out Vector3[] aLeftPortalEndPts, out Vector3[] aRightPortalEndPts)
	{
		aLeftPortalEndPts = roughPath.Clone() as Vector3[];
		aRightPortalEndPts = roughPath.Clone() as Vector3[];
		
		// TODO:
		
		//GridPortalComputer.ComputePortals(roughPath, this, out aLeftPortalEndPts, out aRightPortalEndPts);
	}
	
	public Vector3 GetValidPathFloorPos(Vector3 position)
	{
		return position;
	}
	
	public float GetTerrainHeight(Vector3 position)
	{
		return position.y;
	}
	
	public bool IsInBounds(int index)
	{
		if ( GetWaypoint(index) != null )
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public bool IsInBounds(Vector3 pos)
	{
		return true;
	}
	#endregion
	
	private Waypoint GetWaypoint(int index)
	{
		Waypoint waypoint = null;
		if ( !_waypointDict.TryGetValue( index, out waypoint ) )
		{
			Debug.LogError( "Could not find waypoint with index = " + index.ToString() );
		}
		return waypoint;
	}
	
	private float GetPathWeight(int index)
	{
		return GetWaypoint(index).Weight;
	}
	
	private double[] ConvertToKdPoint(Vector3 pos)
	{
		double[] kdPoint = new double[ kNumKdTreeSortingDimensions ];
		kdPoint[0] = pos.x;
		kdPoint[1] = pos.y;
		kdPoint[2] = pos.z;
		return kdPoint;
	}
}
