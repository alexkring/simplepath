﻿#region Copyright
// ******************************************************************************************
//
// 							SimplePath, Copyright © 2011, Alex Kring
//
// ******************************************************************************************
#endregion

using UnityEngine;
using System.Collections;
using SimpleAI.Navigation;
using SimpleAI;

public class GridOccupancyMapComponent : MonoBehaviour
{
	#region Unity Editor Fields
	public bool								m_rasterizeEveryFrame = true;
	public bool 							m_show = false;
	public Color							m_color = Color.red;
	public GridOccupancyMap.eOccupancyType 	m_mapType = GridOccupancyMap.eOccupancyType.kObstacle;
	public int								m_mapId = 0;
	public float							m_mapWeight = 0f;
	public GridComponent					m_gridComponent;
	#endregion
	
	#region Display Fields
	private GameObject _debugDisplayObject;
	private Texture2D _debugDisplayTexture;
	private Material _debugDisplayMaterial;
	#endregion
	
	#region Fields
	private GridOccupancyMap m_occupancyMap;
	private Grid m_grid;
	#endregion

	#region MonoBehaviour Functions
	private void Start()
	{
		m_grid = m_gridComponent.Grid;
		m_occupancyMap = m_grid.AddOccupancyMap( m_mapType, m_mapId, name, m_mapWeight );
	}
	
	private void Update () 
	{
		if ( m_rasterizeEveryFrame )
		{
			Rasterize();
		}
	}
	
	/// <summary>
	/// Rasterize the obstacles into the grid.
	/// </summary>
	public void Rasterize()
	{
		// Clear the path grid from all blockages
		m_occupancyMap.Clear();
		
		// Render all of the footprints into the path grid as blocked
		FootprintComponent[] footprintArray = (FootprintComponent[])GameObject.FindObjectsOfType(typeof(FootprintComponent));
		foreach (FootprintComponent footprint in footprintArray)
		{	
			if ( footprint.MapId != m_mapId )
			{
				continue;
			}
				
			int numObstructedCells = 0;
			int[] obstructedCells = footprint.GetObstructedCells(m_grid, out numObstructedCells);
			
			for ( int i = 0; i < numObstructedCells; i++ )
			{
				int obstructedCellIndex = obstructedCells[i];
				if ( m_grid.IsInBounds(obstructedCellIndex) )
				{
					m_grid.SetOccupied( m_mapId, obstructedCellIndex, true);
				}
			}
		}	
		
		if ( m_show )
		{
			Render();
		}
	}
	#endregion
	
	private void Render()
	{
		Render( m_grid.Center, m_grid.CellSize, new Vector3(90f, 0f, 0f) );
	}
	
	public void Render(Vector3 centerPosition, float cellSize, Vector3 eulerRotation)
	{	
		if ( _debugDisplayObject == null )
		{
			CreateDebugDisplay();
		}
		
		int numCols = m_occupancyMap.OccupancyArray.GetLength(0);
		int numRows = m_occupancyMap.OccupancyArray.GetLength(1);
		
		for ( int col = 0; col < numCols; col++)
		{
			for ( int row = 0; row < numRows; row++)
			{
				if ( m_occupancyMap.OccupancyArray[col,row] > 0 )
				{
					_debugDisplayTexture.SetPixel( col,row, m_color );
				}
				else
				{
					_debugDisplayTexture.SetPixel( col,row, Color.clear );
				}
			}
		}
		
		_debugDisplayTexture.Apply();
		
		// Positioning
		_debugDisplayObject.transform.position = centerPosition;
		_debugDisplayObject.transform.localRotation = Quaternion.Euler( eulerRotation );
		_debugDisplayObject.transform.localScale = new Vector3( numCols * cellSize, numRows * cellSize, 1f );
	}
	
	private void CreateDebugDisplay()
	{
		_debugDisplayTexture = new Texture2D( 
		                                     m_occupancyMap.OccupancyArray.GetLength(0),
		                                     m_occupancyMap.OccupancyArray.GetLength(1), 
		                                     TextureFormat.ARGB32, 
		                                     false );
		_debugDisplayTexture.filterMode = FilterMode.Point;
		_debugDisplayObject = GameObject.CreatePrimitive(PrimitiveType.Quad);
		_debugDisplayObject.transform.parent = transform;
		_debugDisplayObject.transform.localPosition = Vector3.zero;
		// TODO: need to destroy the MeshCollider on the Quad. Or just really create this object some other way,
		// because the mesh collider is causing performance issues.
		_debugDisplayObject.name = "DebugDisplay";
		_debugDisplayMaterial = new Material( _debugDisplayObject.renderer.material );
		_debugDisplayMaterial.shader = Shader.Find("Transparent/Diffuse");
		MeshRenderer renderer = _debugDisplayObject.GetComponent<MeshRenderer>();
		renderer.material = _debugDisplayMaterial;
		
		_debugDisplayObject.renderer.material.mainTexture = _debugDisplayTexture;
	}
}