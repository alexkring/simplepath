﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class WaypointGraphComponent : PathTerrainComponent 
{
	public float _buildMaxDistanceBetweenNeighbors = 10f;
	public int _maxNumNeighborsPerNode = 5;
	public bool _autoBuild = false;
	public Color _waypointColor = Color.green;
	public Color _edgeColor = Color.white;
	public float _waypointDebugRadius = 0.5f;
	
	public WaypointGraph Graph
	{
		get { return m_pathTerrain as WaypointGraph; }
	}
	
	private void Awake()
	{
		Build( _maxNumNeighborsPerNode, _buildMaxDistanceBetweenNeighbors );
	}
	
	private void Update()
	{
		if ( Application.isEditor && ! Application.isPlaying )
		{
			if ( _autoBuild )
			{
				Build( _maxNumNeighborsPerNode, _buildMaxDistanceBetweenNeighbors );
			}
		}
	}

	private void OnDrawGizmos()
	{
		if ( Graph != null )
		{
			Graph.OnDrawGizmos( _waypointColor, _edgeColor, _waypointDebugRadius );
		}
	}
	
	public void Build( int maxNumNeighborsPerNode, float maxDistanceBetweenNeighbors )
	{
		if ( m_pathTerrain == null )
		{
			m_pathTerrain = new WaypointGraph();
		}
		
		Waypoint[] waypointArray = GameObject.FindObjectsOfType<Waypoint>();
		Graph.Build( waypointArray, maxNumNeighborsPerNode, maxDistanceBetweenNeighbors );
	}
}
