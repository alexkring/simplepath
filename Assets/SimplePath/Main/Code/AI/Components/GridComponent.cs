﻿using UnityEngine;
using System.Collections;
using SimpleAI;

public class GridComponent : PathTerrainComponent 
{
	#region Unity Editor Fields
	public int 				m_numberOfRows = 10;
	public int 				m_numberOfColumns = 10;
	public float 			m_cellSize = 1;
	public bool 			m_debugShow = true;
	public Color			m_debugColor = Color.white;
	#endregion
	
	public Grid Grid
	{
		get { return m_pathTerrain as Grid; }
	}
}
