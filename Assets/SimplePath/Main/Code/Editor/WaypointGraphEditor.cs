﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(WaypointGraphComponent))]
public class WaypointGraphEditor : Editor 
{
	public override void OnInspectorGUI()
	{
		WaypointGraphComponent waypointGraphComponent = (WaypointGraphComponent)target;
		
		EditorGUILayout.BeginVertical();
		
		waypointGraphComponent._buildMaxDistanceBetweenNeighbors = EditorGUILayout.FloatField( 
			"Max neighbor distance", 
			waypointGraphComponent._buildMaxDistanceBetweenNeighbors );
		waypointGraphComponent._maxNumNeighborsPerNode = EditorGUILayout.IntField( 
			"Max neighbors", 
			waypointGraphComponent._maxNumNeighborsPerNode );
		waypointGraphComponent._autoBuild = EditorGUILayout.Toggle("Auto Build", waypointGraphComponent._autoBuild );
		
		if ( waypointGraphComponent._autoBuild )
		{
			
		}
		else
		{
			if ( GUILayout.Button( "Build" ) )
			{
				waypointGraphComponent.Build( waypointGraphComponent._maxNumNeighborsPerNode, waypointGraphComponent._buildMaxDistanceBetweenNeighbors );
			}
		}
		
		EditorGUILayout.EndVertical();
	}
}
